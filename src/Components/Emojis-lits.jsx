import { EmojiItem } from "./Emoji-item";

export function EmojisList({data}){
    //hacer una paginacion para optimizar
        return(
        <div>
            <div className="row justify-content-around py-5">
            {data.slice(0, 50).map((el, index)=><EmojiItem key={index} title={el.title} symbol={el.symbol} keywords={el.keywords}/>)}
            </div>
        </div>
        
    );
}