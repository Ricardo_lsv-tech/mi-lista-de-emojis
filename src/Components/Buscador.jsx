import {useState} from 'react';

export function BuscadorEmojis({filtrar}){
    const [busqueda, setBusqueda]=useState([]);

    const buscador=e=>{
        setBusqueda(e.target.value);
        filtrar(e.target.value);
    }
    return(
        <div className="d-flex justify-content-end">
            <div className="col-4">
            <input 
            type="text"
            className="form-control "
            placeholder="Busca aqui tu emoji favorito..."
            value={busqueda}
            onChange={buscador}
            />
            
            </div>
        </div>
        
    );
}