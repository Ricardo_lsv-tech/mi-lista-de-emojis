export function EmojiItem(props){
    return(
        
        <div className="col-3 text-center">
            <div className="card p-2 m-2">
                <div className="card-header">
                {props.title}
                </div>
                <div className="card-body">
                    <h5 className="card-title">{props.symbol}</h5>
                    <p className="card-text">{props.keywords}</p>
                </div>
            </div>
        </div>
       
    );
}