import './App.css'

import { useEffect, useState } from 'react';

import { BuscadorEmojis } from './Components/Buscador';
import { EmojisList } from './Components/Emojis-lits';
import { TituloEmojis } from './Components/Titulo'

function App() {

  const [data, setData] = useState([]);
  const [filtro, setFiltro] = useState();

  const filtrar=(term)=>{
      let result=data.filter(elemento=>{
        if(elemento.title.toString().toLowerCase().includes(term.toLowerCase())){
            return elemento;
        }
      })
      setFiltro(result);
  }

  const getEmojis= async(url) =>{
    let res = await fetch(url),
        data = await res.json();
        setData(data);
    }

    useEffect(() =>{
        getEmojis('http://localhost:3001/emojis'); 
    },[])

  return (
    <div className="container">
      <TituloEmojis/>
      <BuscadorEmojis filtrar={filtrar}/>
      <EmojisList data={filtro?filtro:data}/>
    </div>
  )
}

export default App
